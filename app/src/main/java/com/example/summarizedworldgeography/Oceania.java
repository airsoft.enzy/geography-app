package com.example.summarizedworldgeography;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class Oceania extends AppCompatActivity {

    private ExampleAdapter adapter;
    private List<ExampleItem> exampleList;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oceania);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getColor(R.color.colorPrimary));
        setSupportActionBar(toolbar);


        fillExampleList();
        setUpRecyclerView();
    }

    private void fillExampleList() {
        exampleList = new ArrayList<>();
        exampleList.add(new ExampleItem(R.drawable.australia,        "Australia",                          "Canberra",         "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.micronesia,       "Estados Federados de Micronesia",    "Palikir",          "Castellano",       "Peso(ARS)",               "45195777 hab",                    "2780400km²",           "Bolivia, Paraguay, Brasil,  Chile, Uruguay y el Océano Atlántico"));
        exampleList.add(new ExampleItem(R.drawable.fiyi,             "Fiji",                               "Suva",             "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.islasmarshall,    "Islas Marshall",                     "Majuro",           "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.islassolomon,     "Islas Salomón",                      "Honiara",          "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.kiribati,         "Kiribati",                           ", Bairiki",        "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.nauru,            "Nauru",                              "Yaren",            "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.nuevazelanda,     "Nueva Zelanda",                      "Wellington",       "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.palaos,           "Palau",                              ", Koror",          "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.papuanuevaguinea, "Papúa Nueva Guinea",                 "Port Moresby",     "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.samoa,            "Samoa",                              "Apia",             "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.tonga,            "Tonga",                              "Nukualofa",        "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.tuvalu,           "Tuvalu",                             "Fongafale",        "",                 "",                        "",                                "",                     ""));
        exampleList.add(new ExampleItem(R.drawable.vanuatu,          "Vanuatu" ,                           "Port Vila",        "",                 "",                        "",                                "",                     ""));



    }

    private void setUpRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new ExampleAdapter(exampleList);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

}
