package com.example.summarizedworldgeography;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private Button btnAmerica;
    private Button btnEuropa;
    private Button btnAsia;
    private Button btnAfrica;
    private Button btnOceania;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnAmerica = (Button) findViewById(R.id.btnAmerica);
        btnAmerica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, America.class);
                startActivity(intent);
            }
        });

        btnEuropa = (Button) findViewById(R.id.btnEuropa);
        btnEuropa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Europa.class);
                startActivity(intent);


            }
        });

        btnAsia = (Button) findViewById(R.id.btnAsia);
        btnAsia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Asia.class);
                startActivity (intent);
            }
        });

        btnAfrica = (Button) findViewById(R.id.btnAfrica);
        btnAfrica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Africa.class);
                startActivity (intent);

            }
        });

        btnOceania = (Button) findViewById(R.id.btnOceania);
        btnOceania.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Oceania.class);
                startActivity (intent);

            }
        });


    }
}
