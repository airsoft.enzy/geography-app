package com.example.summarizedworldgeography;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class Asia extends AppCompatActivity {


    private ExampleAdapter adapter;
    private List<ExampleItem> exampleList;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asia);


        fillExampleList();
        setUpRecyclerView();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getColor(R.color.colorPrimary));
        setSupportActionBar(toolbar);

    }

    private void fillExampleList() {

        exampleList = new ArrayList<>();

        exampleList.add(new ExampleItem(R.drawable.afganistan ,"Afganistán","Kabul" ,"pastún, Persa darí","","","",""));
        exampleList.add(new ExampleItem(R.drawable.arabiasaudita ,"Arabia Saudita","Riad","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.armenia ,"Armenia","Ereván","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.azerbaiyan ,"Azerbaiyán","Bakú","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.banglades ,"Bangladés","Daca","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.barein ,"Baréin","Manama","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.birmaniamyanmar ,"Birmania & Myanmar","Naipyidó","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.brunei ,"Brunéi","Bandar Seri Begawan","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.butan ,"Bután","Timbu","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.camboya ,"Camboya","Nom Pen","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.catar ,"Catar","Doha","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.china ,"China","Pekín","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.chipre ,"Chipre","Nicosia","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.coreadelnorte ,"Corea del Norte","Pionyang","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.coreadelsur ,"Corea del Sur","Seúl","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.emiratesarabesunidos ," Emiratos Árabes Unidos","Abu Dabi","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.filipinas ,"Filipinas","Manila","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.georgia ,"Georgia","Tiflis","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.india ,"India","Nueva Delhi","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.indonesia ,"Indonesia","Yakarta","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.irak ,"Irak","Bagdad","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.iran ,"Irán","Teherán","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.israel ,"Israel","Jerusalén","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.japon ,"Japón","Tokio","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.jordania ,"Jordania","Amán","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.kazajistan ,"Kazajistán","Astaná","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.kirguistan ,"Kirguistán","Biskek","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.kuwait,"Kuwait","Kuwait","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.laos ,"Laos","Vientián","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.libano ,"Líbano","Beirut","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.malasia ,"Malasia","Kuala Lumpur","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.maldivas ,"Maldivas","Malé","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.mongolia ,"Mongolia","Ulán Bator","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.nepal ,"Nepal","Katmandú","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.oman ,"Omán","Mascate","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.pakistan ,"Pakistán","Islamabad","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.rusia ,"Rusia","Moscú","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.singapur ,"Singapur","Singapur","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.siria, "Siria","Damasco","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.sri, "Sri Lanka","Sri Jayawardenapura Kotte","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.tayikistan, "Tayikistán","Dusambé","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.tailandia, "Tailandia","Bangkok","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.timororiental, "Timor Oriental","Dili","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.turkmenistal, "Turkmenistán","Asjabad","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.turquia, "Turquía","Ankara","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.uzbekistan, "Uzbekistán","Taskent","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.vietnam, "Vietnam","Hanói","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.yemen, "Yemen","Saná","","","","",""));

    }

    private void setUpRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new ExampleAdapter(exampleList);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
}
