package com.example.summarizedworldgeography;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;


public class America extends AppCompatActivity {
    private ExampleAdapter adapter;
    private List<ExampleItem> exampleList;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_america);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getColor(R.color.colorPrimary));
        setSupportActionBar(toolbar);



        fillExampleList();
        setUpRecyclerView();

    }


    private void fillExampleList() {
        exampleList = new ArrayList<>();
        exampleList.add(new ExampleItem(R.drawable.antiguaybarbuda, "Antigua y Barbuda", "Saint John","Inglés","Dólar del Caribe-Este","92 436 hab","442,6 km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.argentina, "Argentina", "Buenos Aires","Español","Peso(ARS)","45 195 777 hab","2 780 400km²","Sudamérica"));
        exampleList.add(new ExampleItem(R.drawable.bahamas, "Bahamas", "Nasáu","Inglés","Dólar bahameño(BSD)","392 718 hab","138 801 km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.barbados, "Barbados", "Bridgetown","Inglés","Dólar barbadense (BBD)","279 912 hab","430 km² ","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.belice, "Belice", "Belmopán","Inglés","Dólar beliceño(BZD)","387 879 hab","22 966 km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.bolivia, "Bolivia", "Sucre y La Paz","Español, aimara, guaraní, quechua y otras 33 lenguas","Boliviano (BOB)","11 383 094 hab","1 098 581 km²","Sudamérica"));
        exampleList.add(new ExampleItem(R.drawable.brasil, "Brasil", "Brasilia","Portugués y Lengua de señas","Real (BRL)","212 216 052 hab","8 515 770 km²","Sudamérica"));
        exampleList.add(new ExampleItem(R.drawable.canada, "Canada", "Ottawa","Inglés y francés","Dólar canadiense (CAD)","37 061 011 hab","9 984 670 km²","Norteamérica"));
        exampleList.add(new ExampleItem(R.drawable.chile, "Chile", "Santiago","Español","peso chileno (CLP)","19 107 216 hab","756 102,4 km²","Sudamérica"));
        exampleList.add(new ExampleItem(R.drawable.colombia, "Colombia", "Bogotá","Español","peso colombiano (COP)","50 372 424 hab","1 142 748 km²","Sudamérica"));
        exampleList.add(new ExampleItem(R.drawable.costarica, "Costarica", "San José","Español","colón costarricense (CRC)","5 137 000 hab","51 100 km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.cuba, "Cuba", "La Habana","Español","peso cubano (CUP)","11 221 060 hab","109 8842km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.dominica, "Dominica", "Roseau","Ingles","Dólar del Caribe Oriental (XCD)","69 940 hab","751 km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.ecuador, "Ecuador", "Quito","Español","Dólar estadounidense (USD)","17 300 000 hab","256 370 km²","Sudamérica"));
        exampleList.add(new ExampleItem(R.drawable.elsalvador, "El Salvador", "San Salvador","Español","Dólar estadounidense (USD), Colón salvadoreño (SVC)","6 704 121 hab","21 041 km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.estadosunidos, "Estados Unidos", "Washington D. C.","Ingles","Dólar estadounidense (USD)","325 719 178 hab","9 147 593 km²","Norteamérica"));
        exampleList.add(new ExampleItem(R.drawable.granada, "Granada", "Saint George","Inglés","Dólar Caribe-Este (XCD)","109 590 hab","344 km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.guatemala, "Guatemala", "Ciudad de Guatemala","Español","Quetzal (GTQ)","16 301 286 hab","108 889 km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.guyana, "Guyana", "Georgetown","Inglés","Dólar guyanés (GYD)","801 193 hab","214 969 km²","Sudamérica"));
        exampleList.add(new ExampleItem(R.drawable.haiti, "Haiti", "Puerto Príncipe","Criollo haitiano y francés","Gourde haitiano (HTG)","11 325 861 hab","27 750 km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.honduras, "Honduras", "Tegucigalpa","Español","Lempira (HNL)","9 195 012 hab","112 492 km²","Centroamérica"));
        exampleList.add(new ExampleItem(R.drawable.jamaica, "Jamaica", "Kingston","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.mexico, "Mexico", "Ciudad de México","","","","","Norteamérica"));
        exampleList.add(new ExampleItem(R.drawable.nicaragua, "Nicaragua", "Managua","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.panama, "Panama", "Ciudad de Panamá","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.paraguay, "Paraguay", "Asunción","","","","","Sudamérica"));
        exampleList.add(new ExampleItem(R.drawable.peru, "Peru", "Lima","","","","","Sudamérica"));
        exampleList.add(new ExampleItem(R.drawable.republicadominicana, "Republica Dominicana", "Santo Domingo","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.sancristobalynieves, "San Cristobal y Nieves", "Basseterre","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.sanvicenteylasgranadinas, "San Vicente y Las Granadinas", "Kingstown","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.santalucia, "Santa Lucia", "Castries","","","","",  ""));
        exampleList.add(new ExampleItem(R.drawable.surinam, "Surinam", "Paramaribo","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.trinidadytobago, "Trinidad y Tobago", "Puerto España","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.uruguay, "Uruguay", "Montevideo","","","","","Sudamérica"));
        exampleList.add(new ExampleItem(R.drawable.venezuela, "Venezuela", "Caracas","","","","","Sudamérica"));

    }

    private void setUpRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new ExampleAdapter(exampleList);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
}