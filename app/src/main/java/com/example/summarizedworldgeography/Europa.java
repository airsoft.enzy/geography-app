package com.example.summarizedworldgeography;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class Europa extends AppCompatActivity {

    private ExampleAdapter adapter;
    private List<ExampleItem> exampleList;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_europa);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getColor(R.color.colorPrimary));
        setSupportActionBar(toolbar);



        fillExampleList();
        setUpRecyclerView();
    }

    private void fillExampleList() {
        exampleList = new ArrayList<>();

        exampleList.add(new ExampleItem(R.drawable.albania, "Albania", "Tirana", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.alemania,"Alemania","Berlín", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.andorra, "Andorra", "Andorra la Vieja", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.armenia, "Armenia", "Ereván", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.austria, "Austria",        "Viena", "", "", "", "", ""));

        exampleList.add(new ExampleItem(R.drawable.azerbaiyan, "Azerbaiyán",        "Bakú", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.belgica, "Belgica",        "Bruselas", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.bielorrusia, "Bielorrusia",               "Bielorrusia", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.bosnizayherzegovina, "Bosnia-Herzegovina","Sarajevo", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.bulgaria, "Bulgaria",                     "Sofía", "", "", "", "", ""));

        exampleList.add(new ExampleItem(R.drawable.chipre, "Chipre",                       "Nicosia", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.vaticano, "Ciudad del Vaticano",        "Ciudad del Vaticano", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.croacia, "Croacia",                     "Zagreb", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.dinamarca, "Dinamarca",                 "Copenhague", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.austria, "Eslovaquia",                  "Bratislava", "", "", "", "", ""));

        exampleList.add(new ExampleItem(R.drawable.eslovenia, "Eslovenia",                 "Liubliana", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.espana, "España",                       "Madrid", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.estonia, "Estonia",                     "Tallin", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.rusia, "Federación Rusa",              "Moscú", "", "", "", "", ""));

        exampleList.add(new ExampleItem(R.drawable.finlandia, "Finlandia",                 "Helsinki", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.francia, "Francia",                       "París", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.georgia, "Georgia",                     "Tiflis", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.hungria, "Hungría",                       "Budapest", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.espana, "España",                       "Madrid", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.irlanda, "Irlanda",                     "Dublín", "", "", "", "", ""));

        exampleList.add(new ExampleItem(R.drawable.islandia, "Islandia",                   "Reikiavik", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.italia, "Italia",                       "Roma", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.kazajistan, "Kazajistán",               "Astana", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.letonia, "Letonia",                     "Riga", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.liechtenstein, "Liechtenstein",                       "Vaduz", "", "", "", "", ""));

        exampleList.add(new ExampleItem(R.drawable.lituania, "Lituania",                 "Vilna", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.luxemburgo, "Luxemburgo",                       "v", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.macedonia, "Macedonia del Norte",                     "Skopie", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.malta, "Malta",                       " La Valeta", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.moldavia, "Moldova",                       "Chisináu", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.monaco, "Mónaco",                     "Mónaco", "", "", "", "", ""));

        exampleList.add(new ExampleItem(R.drawable.montenegro, "Montenegro",                 "Podgorica", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.italia, "Italia",                       "Roma", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.kazajistan, "Kazajistán",                     "Astana", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.letonia, "Letonia",                       "Riga", "", "", "", "", ""));

        exampleList.add(new ExampleItem(R.drawable.noruega, "Noruega",                 "Oslo", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.luxemburgo, "Países Bajos",                       "Ámsterdam", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.macedonia, "Polonia",                     "Varsovia", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.malta, "Portugal",                       " Lisboa", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.moldavia, "Reino Unido",                       "Londres", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.monaco, "República Checa",                     "Praga", "", "", "", "", ""));

        exampleList.add(new ExampleItem(R.drawable.montenegro, "Rumania",                 "Bucarest", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.italia, "San Marino",                       "San Marino", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.kazajistan, "Serbia",                     "Belgrado", "", "", "", "", ""));
        exampleList.add(new ExampleItem(R.drawable.suecia, "Suecia","Estocolmo","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.suiza, "Suiza","Bernal","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.ucrania, "Ucrania","Kiev","","","","",""));

    }

    private void setUpRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new ExampleAdapter(exampleList);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
}