package com.example.summarizedworldgeography;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class Africa extends AppCompatActivity {



    private ExampleAdapter adapter;
    private List<ExampleItem> exampleList;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_africa);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getColor(R.color.colorPrimary));
        setSupportActionBar(toolbar);


        fillExampleList();
        setUpRecyclerView();
    }



    private void fillExampleList() {
        exampleList = new ArrayList<>();
        exampleList.add(new ExampleItem(R.drawable.angola, "Angola", "Luanda","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.argelia, "Argelia", "Argel","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.benin, "Benín", "Porto – novo","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.botsuana, "Botsuana", "Gaborone","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.burkinafaso, "Burkina Faso", "Uagadugú","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.burundi, "Burundi", "Buyumbura","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.caboverde, "Cabo Verde", "Praia","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.camerun, "Camerún", "Yaundé","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.republicacentroafricana, "Centroafricana (República)", "Bangui","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.comoras, "Comores", "Moroni","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.costademarfil, "Costa de Marfil", "Yamusukro","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.chad, "Chad", "Yamena","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.egipto, "Egipto", "El Cairo","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.eritrea, "Eritrea", "Asmara","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.etiopia, "Etiopía", "Addis Abeba","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.gabon, "Gabón", "Libre Ville","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.gambia, "Gambia", "Banjul","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.ghana, "Ghana", "Accra","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.guinea, "Guinea", "Conakry","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.guineabisau, "Guinea-Bissau", "Bissau","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.guineaecuatorial, "Guinea Ecuatorial", "Malabo","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.kenia, "Kenia", "Nairobi","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.lesoto, "Lesoto", "Maseru","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.liberia, "Liberia", "Monrovia","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.libia, "Libia", "Tripoli","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.madagascar, "Madagascar", "Antananarivo","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.malaui, "Malaui", "Lilongüe","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.mali, "Malí", "Bamako","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.marruecos, "Marruecos", "Rabat","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.mauricio, "Mauricio", "Port Louis","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.mauritania, "Mauritania", "Nuakchot","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.mozambique, "Mozambique", "Maputo","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.namibia, "Namibia", "Windhoek1","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.niger, "Níger", "Niamey","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.nigeria, "Nigeria", "Abuya","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.ruanda ,"Ruanda","Kigali","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.republicadelcongo ,"República del Congo","Brazzaville","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.republicademocraticadelcongo ,"República Democrática del congo","Kinshasa","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.santomeyprincipe ,"Santo Tomé y Príncipe","Santo Tomé","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.senegal ,"Senegal","Dakar","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.seychelles ,"Seychelles","Victoria","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.sierraleona ,"Sierra Leona","Freetown","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.somalia ,"Somalia","Mogadiscio","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.sudafrica ,"Sudáfrica (República)","Pretoria","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.sudan ,"Sudán","Jartum","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.sudandelsur ,"Sudán del Sur","Yuba","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.suazilandia ,"Suazilandia","Lobamba","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.tanzania ,"Tanzania","Dar el Salam","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.tunez ,"Túnez","Túnez","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.togo ,"Togo","Lomé","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.uganda ,"Uganda","Kampala","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.yibuti ,"Yibuti","Yibuti","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.zambia ,"Zambia","Lusaka","","","","",""));
        exampleList.add(new ExampleItem(R.drawable.zimbabue ,"Zimbabue","Hare","","","","",""));



        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,
        //
        //,





    }

    private void setUpRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new ExampleAdapter(exampleList);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

}
